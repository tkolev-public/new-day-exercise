To package and run tests:
```
sbt assembly
```

The application takes two command-line arguments - 1st is the input directory and 2nd is the output directory e.g

```
./spark-submit --class SparkApp path-to-project/target/scala-2.12/new-day-exercise-assembly-0.1.jar ~/Documents/tmp/ml-1m/ ~/Documents/tmp/new-day-ouput
```