import org.apache.spark.sql.SparkSession

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

object SparkApp extends App {
  val fromDir = args(0)
  val toDir = args(1)

  implicit val spark: SparkSession = SparkSession.builder
    .master("local[*]")
    .appName("new-day-exercise-app")
    .getOrCreate

  def loadDatFile(path: String) =
    spark
      .read
      .option("delimiter", "::")
      .csv(path)

  val MoviesColumns = Seq("movieId", "title", "genres")
  val RatingsColumns = Seq("userId", "movieId", "rating", "timestamp")

  val movies = loadDatFile(s"$fromDir/movies.dat").toDF(MoviesColumns: _*)
  val ratings = loadDatFile(s"$fromDir/ratings.dat").toDF(RatingsColumns: _*)
  val movieRatings = new MovieService(spark).computeMovieRatings(movies, ratings)
  val usersTop3Movies = new UserService(spark).computeUsersTop3Movies(ratings)

  Await.ready(Future.sequence(
    Seq(
      Future(movies.write.parquet(s"$toDir/movies")),
      Future(ratings.write.parquet(s"$toDir/ratings")),
      Future(movieRatings.write.parquet(s"$toDir/movieRatings")),
      Future(usersTop3Movies.write.parquet(s"$toDir/usersTop3Movies"))
    )
  ), Duration.Inf)
}
