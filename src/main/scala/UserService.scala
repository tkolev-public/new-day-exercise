import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._

class UserService(spark: SparkSession) {

  import spark.sqlContext.implicits._

  private val topN = 3

  private val WindowSpec = Window
    .partitionBy("userId")
    .orderBy(desc("rating"))

  def computeUsersTop3Movies(ratings: DataFrame): DataFrame = {
    ratings
      .withColumn("rank", rank.over(WindowSpec))
      .where(s"rank <= $topN")
      .groupBy('userId)
      .agg(collect_list('movieId).as(s"topMovies"))
  }
}
