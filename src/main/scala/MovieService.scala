import org.apache.spark.sql.functions.{max, min, _}
import org.apache.spark.sql.types.{DoubleType, IntegerType}
import org.apache.spark.sql.{DataFrame, SparkSession}

class MovieService(spark: SparkSession) {

  import spark.sqlContext.implicits._

  def computeMovieRatings(movies: DataFrame, ratings: DataFrame): DataFrame = {
    movies
      .join(ratings, Seq("movieId"), "left")
      .groupBy("movieId", "title", "genres")
      .agg(
        min('rating).cast(IntegerType) as "minRating",
        max('rating).cast(IntegerType) as "maxRating",
        avg('rating).cast(DoubleType) as "avgRating"
      )
  }
}
