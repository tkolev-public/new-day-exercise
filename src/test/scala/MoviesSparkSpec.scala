import org.scalatest.flatspec.AnyFlatSpec

class MoviesSparkSpec extends AnyFlatSpec  {
  import SparkTest._
  import spark.implicits._

  it should "Left join movies with ratings and calculate min, max and avg ratings for each movie" in {

    val movieService = new MovieService(spark)
    val movies = sc.parallelize(List(
      (BigInt(1), "Movie1", "Genre1|Genre2"),
      (BigInt(2), "Movie2", "Genre3"),
      (BigInt(3), "Movie3", "Genre2"),
    ))
      .toDF(Seq("movieId", "title", "genres"): _*)

    val ratings = sc.parallelize(List(
      (BigInt(1), BigInt(1), 5, 978300760),
      (BigInt(2), BigInt(1), 4, 978300760),
      (BigInt(2), BigInt(2), 3, 978300760)
    ))
      .toDF(Seq("userId", "movieId", "rating", "timestamp"): _*)

    val expectedDf = sc.parallelize(List(
      (BigInt(1), "Movie1", "Genre1|Genre2",BigInt(4), BigInt(5), BigDecimal(4.5)),
      (BigInt(3), "Movie3", "Genre2"       ,null     , null     , null),
      (BigInt(2), "Movie2", "Genre3"       ,BigInt(3), BigInt(3), BigDecimal(3.0))
    ))
      .toDF(Seq("movieId", "title", "genres") ++ Seq("minRating", "maxRating", "avgRating"): _*)

    val df = movieService.computeMovieRatings(movies, ratings)
    assert(!df.isEmpty && df.except(expectedDf).isEmpty)
  }

  it should "Compute each user's top 3 movies based on ratings given" in {
    val ratings = sc.parallelize(List(
      (BigInt(1), BigInt(1), 5, 978300760),
      (BigInt(1), BigInt(2), 4, 978300760),
      (BigInt(1), BigInt(3), 3, 978300760),
      (BigInt(1), BigInt(4), 5, 978300760),
      (BigInt(2), BigInt(1), 3, 978300760),
      (BigInt(2), BigInt(2), 4, 978300760),
    ))
      .toDF(Seq("userId", "movieId", "rating", "timestamp"): _*)

    val expectedDf = sc.parallelize(List(
      (BigInt(1), List(1, 4, 2)),
      (BigInt(2), List(2, 1))
    ))
      .toDF(Seq("userId", "topMovies"): _*)

    val df = new UserService(spark).computeUsersTop3Movies(ratings)
    assert(!df.isEmpty && df.except(expectedDf).isEmpty)
  }
}
