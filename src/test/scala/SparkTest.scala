import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession

object SparkTest {
  val spark: SparkSession =
    SparkSession
      .builder()
      .appName("app-name")
      .master("local[*]")
      .getOrCreate()
  val sc: SparkContext = spark.sparkContext
}
